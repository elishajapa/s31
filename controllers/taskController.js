// importing the models
const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task ({
		// Sets the "name" property with the value reveived from the client/Postman
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	})
}

// Controller function for deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

// Contoller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}

		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr)
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}

// Activity 
module.exports.getOneTask = (Id) => {
	return Task.findById(Id).then((updateTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return updateTask;
		}
	})
}

module.exports.updateStatus = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}

		result.status = "Complete";
		return result.save().then((updatedTask, Err) => {
			if (Err){
				console.log(Err)
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}