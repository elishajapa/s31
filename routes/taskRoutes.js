// Setup express dependency
const express = require("express");
// Create a Router instance
const router = express.Router();
// import TaskController.js
const taskController = require("../controllers/taskController");

// Routes

// Route to get all tasks
router.get("/", (req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})

// Route to create a new task 
router.post("/", (req,res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for deleting a task
// loclahost:3001/tasks/09876b0987098y0
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Router for updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})
// Activity
router.get("/:id", (req,res) => {
	taskController.getOneTask(req.params.id).then(result => res.send(
		result));
})  

router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})
module.exports = router;

